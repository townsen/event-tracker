# Event Tracker

This repository contains CloudFormation templates and code artifacts for a prototypical
system that sends an SMS message when an event is added to a DynamoDB table.
It provides a website that allows the recipient of the message to see all records with the
same EventId. It uses a simple 'generic' model: EventId, EventDate, Description and Mobile.

## Diagram

This diagram shows the overall system architecture:

![Event Tracker Design](doc/Sendium.png)

### Pre-requisite Manual Steps

Some steps cannot be automated in the CloudFormation template as they require names
that may not be available:

* The Site Domain Name: acquire this manually using the Route53 console.
* Choose the subdomain that the site will appear under: The default is 'events'
* Create an S3 Bucket with the name of the fully qualified subdomain: `events.<domain>`
* Configure the bucket for Web Hosting and disable Block Public Access from both new or
  any public bucket or access point policies.
* Copy the following files to the bucket using either the console or with the commands:

```bash
  aws s3 cp src/index.html s3://<bucket>/
  aws s3 cp src/error.html s3://<bucket>/
  aws s3 cp src/simple.css s3://<bucket>/
  aws s3 cp images/logo.png s3://<bucket>/
```

## Building using CloudFormation

The Event tracking system is built using a [cloudformation
template](cloudformation/event_tracker.yaml). This takes the following parameters, most of
which are correctly defaulted (values in parentheses):

* The name of the DynamoDB table (Events)
* The Site Name used in SMS Messages and the Website Title (Sendium)
* The Domain Name without a trailing period (sendium.net) It is assumed that this
  domain is hosted by Route 53 and that the account has permissions to update the zone
* The Subdomain of the API site in unqualified form (events)
* The name of an AWS Tag to apply to each resource (Project)
* The value of the above AWS Tag (EventTracker)

Use the CloudFormation console to create a stack with new resources and select the
template file `cloudformation/event_tracker.yaml`.

## Costs and Quotas

The API Gateway used has a free tier of one million API calls per month for up to 12
months.

Note that the SMS sending API has a default limit of $1 per month. Increase this by going
to the [Service
Quotas](https://eu-west-1.console.aws.amazon.com/servicequotas/home?region=eu-west-1#!/services/sns/quotas)

Transactional SMS's sent from the Dublin Region currently cost 4c each. So a quota of $100 per month allows 2,500 mesages.

